<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$consonantsBel = isset($_POST['consonantsBel']) ? $_POST['consonantsBel'] : '';
	$sonorantBel = isset($_POST['sonorantBel']) ? $_POST['sonorantBel'] : '';
	$obstruentBel = isset($_POST['obstruentBel']) ? $_POST['obstruentBel'] : '';
	$hissingBel = isset($_POST['hissingBel']) ? $_POST['hissingBel'] : '';
	$hushingBel = isset($_POST['hushingBel']) ? $_POST['hushingBel'] : '';
	$iotationBel = isset($_POST['iotationBel']) ? $_POST['iotationBel'] : '';
	$delimiters = isset($_POST['delimiters']) ? $_POST['delimiters'] : '';
	$checkbox1 = isset($_POST['checkbox1']) ? $_POST['checkbox1'] : 0;
	$checkbox2 = isset($_POST['checkbox2']) ? $_POST['checkbox2'] : 0;
	$checkbox3 = isset($_POST['checkbox3']) ? $_POST['checkbox3'] : 0;
	$checkbox4 = isset($_POST['checkbox4']) ? $_POST['checkbox4'] : 0;
	$checkbox5 = isset($_POST['checkbox5']) ? $_POST['checkbox5'] : 0;
	$checkbox6 = isset($_POST['checkbox6']) ? $_POST['checkbox6'] : 0;
	$selectedCheckboxes = array('checkbox1' => $checkbox1, 'checkbox2' => $checkbox2, 'checkbox3' => $checkbox3, 'checkbox4' => $checkbox4, 'checkbox5' => $checkbox5, 'checkbox6' => $checkbox6);
	$setting1 = isset($_POST['setting1']) ? $_POST['setting1'] : 0;
	$selectedSettings = array('setting1' => $setting1);
	
	include_once 'PhoneticPhenomenaSearcher.php';
	PhoneticPhenomenaSearcher::loadLocalization($localization);

	$msg = '';
	if(!empty($text))
	{
		$PhoneticPhenomenaSearcher = new PhoneticPhenomenaSearcher($selectedCheckboxes, $selectedSettings);
		$PhoneticPhenomenaSearcher->setText($text);
		$PhoneticPhenomenaSearcher->setConsonants($consonantsBel);
		$PhoneticPhenomenaSearcher->setSonorant($sonorantBel);
		$PhoneticPhenomenaSearcher->setObstruent($obstruentBel);
		$PhoneticPhenomenaSearcher->setHissing($hissingBel);
		$PhoneticPhenomenaSearcher->setHushing($hushingBel);
		$PhoneticPhenomenaSearcher->setIotation($iotationBel);
		$PhoneticPhenomenaSearcher->setDelimiters($delimiters);
		$PhoneticPhenomenaSearcher->run();
		$PhoneticPhenomenaSearcher->saveCacheFiles();
		
		$result['text'] = $text;
		$result['statistics'] = $PhoneticPhenomenaSearcher->getStatisticsArr();
		$result['result'] = $PhoneticPhenomenaSearcher->getResult();
		$msg = json_encode($result);
	}
	echo $msg;
?>