<?php
	class PhoneticPhenomenaSearcher
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $selectedCheckboxes = array();
		private $selectedSettings = array();
		private $result = '';
		private $resultArr = array();
		private $statisticsArr = array();

		private $apostrophesArr = array("'", "ʼ", "’", "‘");
		private $acuteAccentsArr = array("´", "\xcc\x81");
		private $graveAccent = "\xcc\x80";
		private $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї';
		private $lettersBel = 'АБВГДЕЁЖЗІЙКЛМНОПРСТУЎФХЦЧШЫЬЭЮЯабвгдеёжзійклмнопрстуўфхцчшыьэюя';
		private $consonantsBel = 'БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтЎўФфХхЦцЧчШшЬь';
		private $sonorantBel = 'ВвЙйЛлМмНнРрЎў';
		private $obstruentBel = 'БбГгДдЖжЗзКкПпСсТтФфХхЦцЧчШш';
		private $hissingBel = 'ЗзСсЦц';
		private $hushingBel = 'ЖжЧчШш';
		private $iotationBel = 'ЕеЁёЮюЯяЙй';
		private $delimiters = array('/');
		private $punctuation = array('.', ',', '-', '–', '—', ':', ';', '(', ')', '“', '”', '"', "\r\n", '...', '!', '?');
		
		// санорныя (sonorant): [в], [в’], [й], [л], [л’], [м], [м’], [н], [н’], [р], [ў]
		// шумныя (obstruent): [б], [б’], [г], [г’], [г´], [г´’], [з], [з’], [д], [дз], [дз’], [ж], [дж], [к], [к’], [п], [п’], [с], [с’], [т], [ф], [ф’], [х], [х’], [ц], [ц’], [ч], [ш]
		// свісцячыя (hissing): [с], [з], [ц], [дз], [с’], [з’], [ц’], [дз’]
		// шыпячыя (hushing): [ш], [ж], [ч], [дж]
		
		const BR = "<br>\n";
		
		function __construct($selectedCheckboxes = array(), $selectedSettings = array())
		{
			$this->selectedCheckboxes = $selectedCheckboxes;
			$this->selectedSettings = $selectedSettings;
		}

		public function setText($text) { $this->text = $text; }
		public function setConsonants($text) { $this->consonantsBel = $text; }
		public function setSonorant($text) { $this->sonorantBel = $text; }
		public function setObstruent($text) { $this->obstruentBel = $text; }
		public function setHissing($text) { $this->hissingBel = $text; }
		public function setHushing($text) { $this->hushingBel = $text; }
		public function setIotation($text) { $this->iotationBel = $text; }
		public function setDelimiters($text) { $this->delimiters = preg_split('//u', $text, -1, PREG_SPLIT_NO_EMPTY); }
		
		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Phonetic Phenomena Searcher';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/PhoneticPhenomenaSearcher/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		private function defineWords($string, $characterGroup1, $characterGroup2)
		{
			$wordsArr = array();
			if(!empty($characterGroup1))
			{
				$pattern = "/(^|[$characterGroup1][$characterGroup1$characterGroup2]*)([^$characterGroup1]*)/u";
				preg_match_all($pattern, $string, $wordsArr, PREG_SET_ORDER);
			}			
			return $wordsArr;
		}
		
		public function run()
		{
			mb_internal_encoding('UTF-8');
			$this->result = str_replace("\n", "\n<br>", $this->text);
			$this->paragraphsArr = explode("\n", $this->text);
			foreach($this->paragraphsArr as $paragraph)
			{
				$paragraph = trim($paragraph);
				if(!empty($paragraph))
				{
					$mainChars = $this->letters;
					$additionalChars = implode('', $this->acuteAccentsArr) . implode('', $this->apostrophesArr) . $this->graveAccent . '\-';
					$wordsArr = $this->defineWords($paragraph, $mainChars, $additionalChars);
					$previousWord = $previousDelimiter = '';
					
					for($i=0; $i<count($wordsArr); $i++)
					{
						$word = isset($wordsArr[$i][1]) ? $wordsArr[$i][1] : '';
						$delimiter = isset($wordsArr[$i][2]) ? $wordsArr[$i][2] : '';
						if(!empty($word))
						{
							if(!empty($previousWord))
							{
								if(!empty($this->selectedSettings['setting1']))
								{
									foreach($this->delimiters as $del)
									{
										if(mb_strpos($previousDelimiter, $del) !== false)
										{
											$previousWord = $word;
											$previousDelimiter = $delimiter;
											continue 2;
										}
									}
								}
								$leftLetter = mb_substr($previousWord, -1);
								if($leftLetter == 'ь' || $leftLetter == 'Ь')
								{
									$softSign = $leftLetter;
									$leftLetter = mb_substr($previousWord, -2, 1);
								}
								else
								{
									$softSign = '';
								}
								$rightLetter = mb_substr($word, 0, 1);
								$search = $leftLetter . $softSign . $previousDelimiter . $rightLetter;
								/* праверка па сьпісе фанетычных з’яў */
								if(!empty($this->selectedCheckboxes['checkbox6']))
								{
									if(mb_strpos($this->consonantsBel, $leftLetter) !== false && mb_strpos($this->consonantsBel, $rightLetter) !== false && $leftLetter === $rightLetter) {
										$this->result = str_replace($search, '<span style="background: #FFCC99">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['FFCC99'])) {
											$this->statisticsArr['FFCC99'] = 1;
										} else {
											$this->statisticsArr['FFCC99']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
								if(!empty($this->selectedCheckboxes['checkbox4'])) {
									if(mb_strpos($this->hissingBel . $this->hushingBel, $leftLetter) !== false && mb_strpos($this->hissingBel . $this->hushingBel, $rightLetter) !== false) {
										$this->result = str_replace($search, '<span style="background: #FFCCCC">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['FFCCCC'])) {
											$this->statisticsArr['FFCCCC'] = 1;
										} else {
											$this->statisticsArr['FFCCCC']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
								if(!empty($this->selectedCheckboxes['checkbox3'])) {
									if(mb_strpos($this->obstruentBel, $leftLetter) !== false && mb_strpos($this->obstruentBel, $rightLetter) !== false) {
										$this->result = str_replace($search, '<span style="background: #CCCCFF">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['CCCCFF'])) {
											$this->statisticsArr['CCCCFF'] = 1;
										} else {
											$this->statisticsArr['CCCCFF']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
								if(!empty($this->selectedCheckboxes['checkbox2'])) {
									if(mb_strpos($this->sonorantBel, $leftLetter) !== false && mb_strpos($this->sonorantBel, $rightLetter) !== false) {
										$this->result = str_replace($search, '<span style="background: #CCFFFF">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['CCFFFF'])) {
											$this->statisticsArr['CCFFFF'] = 1;
										} else {
											$this->statisticsArr['CCFFFF']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
								if(!empty($this->selectedCheckboxes['checkbox1']))
								{
									if(mb_strpos($this->consonantsBel, $leftLetter) !== false && mb_strpos($this->consonantsBel, $rightLetter) !== false) {
										$this->result = str_replace($search, '<span style="background: #CCFFCC">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['CCFFCC'])) {
											$this->statisticsArr['CCFFCC'] = 1;
										} else {
											$this->statisticsArr['CCFFCC']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
								if(!empty($this->selectedCheckboxes['checkbox5']))
								{
									if(mb_strpos($this->iotationBel, $leftLetter) !== false && mb_strpos($this->iotationBel, $rightLetter) !== false) {
										$this->result = str_replace($search, '<span style="background: #FFFCCC">'. $search .'</span>', $this->result);
										if(!isset($this->statisticsArr['FFFCCC'])) {
											$this->statisticsArr['FFFCCC'] = 1;
										} else {
											$this->statisticsArr['FFFCCC']++;
										}
										$previousWord = $word;
										$previousDelimiter = $delimiter;
										continue;
									}
								}
							}
							$previousWord = $word;
							$previousDelimiter = $delimiter;
						}
					}
				}
			}
		}
		
		public function saveCacheFiles()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'PhoneticPhenomenaSearcher';
			$sendersName = 'Phonetic Phenomena Searcher';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300)
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				else
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult() {
			return $this->result;
		}
		
		public function getStatisticsArr() {
			return $this->statisticsArr;
		}
	}
?>