<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'PhoneticPhenomenaSearcher.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = PhoneticPhenomenaSearcher::loadLanguages();
	PhoneticPhenomenaSearcher::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo PhoneticPhenomenaSearcher::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', PhoneticPhenomenaSearcher::showMessage('default input'))); ?>";
			
			function ajaxRequest() {
				var checkbox1 = $('input:checkbox[name=checkbox1]').prop('checked') == true ? 1 : 0;
				var checkbox2 = $('input:checkbox[name=checkbox2]').prop('checked') == true ? 1 : 0;
				var checkbox3 = $('input:checkbox[name=checkbox3]').prop('checked') == true ? 1 : 0;
				var checkbox4 = $('input:checkbox[name=checkbox4]').prop('checked') == true ? 1 : 0;
				var checkbox5 = $('input:checkbox[name=checkbox5]').prop('checked') == true ? 1 : 0;
				var checkbox6 = $('input:checkbox[name=checkbox6]').prop('checked') == true ? 1 : 0;
				var setting1 = $('input:checkbox[name=setting1]').prop('checked') == true ? 1 : 0;
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/PhoneticPhenomenaSearcher/api.php',
					data: {
						'localization': '<?php echo $localizationLang; ?>',
						'text': $('textarea#inputTextId').val(),
						'consonantsBel': $("input:text[name=consonantsBel]").val(),
						'sonorantBel': $("input:text[name=sonorantBel]").val(),
						'obstruentBel': $("input:text[name=obstruentBel]").val(),
						'hissingBel': $("input:text[name=hissingBel]").val(),
						'hushingBel': $("input:text[name=hushingBel]").val(),
						'iotationBel': $("input:text[name=iotationBel]").val(),
						'delimiters': $("input:text[name=delimiters]").val(),
						'checkbox1': checkbox1,
						'checkbox2': checkbox2,
						'checkbox3': checkbox3,
						'checkbox4': checkbox4,
						'checkbox5': checkbox5,
						'checkbox6': checkbox6,
						'setting1': setting1
					},
					success: function(msg) {
						var result = jQuery.parseJSON(msg);
						var output = '<br /><b><?php echo PhoneticPhenomenaSearcher::showMessage('statistics'); ?>:</b>';
						for (var key in result.statistics){
							if (typeof result.statistics[key] !== 'function') {
								output += '&nbsp;&nbsp;&nbsp;<span style="background: #' + key + '">' + result.statistics[key] + '</span>';
							}
						}
						output += '<br /><br />';
						output += result.result;
						$('#resultId').html(output);
					},
					error: function() {
						$('#resultId').html('ERROR');
					}
				});
			}
			
			$(document).ready(function () {
				$('button#MainButtonId').click(function(){
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					ajaxRequest();				
				});
				$(document).on('keypress', 'input[type=text]', function (e) {
					if (e.which == 13) {
						$('#resultBlockId').show('slow');
						$('#resultId').empty();
						$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
						ajaxRequest();
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/PhoneticPhenomenaSearcher/?lang=<?php echo $lang; ?>"><?php echo PhoneticPhenomenaSearcher::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo PhoneticPhenomenaSearcher::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo PhoneticPhenomenaSearcher::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = PhoneticPhenomenaSearcher::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . PhoneticPhenomenaSearcher::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo PhoneticPhenomenaSearcher::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo PhoneticPhenomenaSearcher::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo PhoneticPhenomenaSearcher::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", PhoneticPhenomenaSearcher::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group row">
						<label for="consonantsBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('consonants'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="consonantsBel" id="consonantsBelId" value="БбВвГгДдЖжЗзЙйКкЛлМмНнПпРрСсТтЎўФфХхЦцЧчШшЬь">
						</div>
					</div>
					<div class="form-group row">
						<label for="sonorantBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('sonorant'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="sonorantBel" id="sonorantBelId" value="ВвЙйЛлМмНнРрЎў">
						</div>
					</div>
					<div class="form-group row">
						<label for="obstruentBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('obstruent'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="obstruentBel" id="obstruentBelId" value="БбГгДдЖжЗзКкПпСсТтФфХхЦцЧчШш">
						</div>
					</div>
					<div class="form-group row">
						<label for="hissingBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('hissing'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="hissingBel" id="hissingBelId" value="ЗзСсЦц">
						</div>
					</div>
					<div class="form-group row">
						<label for="hushingBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('hushing'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="hushingBel" id="hushingBelId" value="ЖжЧчШш">
						</div>
					</div>
					<div class="form-group row">
						<label for="iotationBelId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('iotation'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="iotationBel" id="iotationBelId" value="ЕеЁёЮюЯяЙй">
						</div>
					</div>
					<div class="form-group row">
						<label for="delimitersId" class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('delimiters'); ?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="delimiters" id="delimitersId" value="/">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('checkboxes'); ?></label>
						<div class="col-sm-10">
							<?php $selectedCheckboxes = array(); ?>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox4" id="checkbox4Id" value="1" checked>
								<label class="form-check-label" for="checkbox4Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox4'); ?>
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox3" id="checkbox3Id" value="1" checked>
								<label class="form-check-label" for="checkbox3Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox3'); ?>
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox2" id="checkbox2Id" value="1" checked>
								<label class="form-check-label" for="checkbox2Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox2'); ?>
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox1" id="checkbox1Id" value="1">
								<label class="form-check-label" for="checkbox1Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox1'); ?>
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox5" id="checkbox5Id" value="1">
								<label class="form-check-label" for="checkbox5Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox5'); ?>
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="checkbox6" id="checkbox6Id" value="1">
								<label class="form-check-label" for="checkbox6Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('checkbox6'); ?>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><?php echo PhoneticPhenomenaSearcher::showMessage('settings'); ?></label>
						<div class="col-sm-10">
							<?php $selectedSettings = array(); ?>
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="setting1" id="setting1Id" value="1" checked>
								<label class="form-check-label" for="setting1Id">
									<?php echo PhoneticPhenomenaSearcher::showMessage('setting1'); ?>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo PhoneticPhenomenaSearcher::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo PhoneticPhenomenaSearcher::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo PhoneticPhenomenaSearcher::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/PhoneticPhenomenaSearcher" target="_blank"><?php echo PhoneticPhenomenaSearcher::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/PhoneticPhenomenaSearcher/-/issues/new" target="_blank"><?php echo PhoneticPhenomenaSearcher::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo PhoneticPhenomenaSearcher::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo PhoneticPhenomenaSearcher::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $localizationLang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo PhoneticPhenomenaSearcher::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php PhoneticPhenomenaSearcher::sendErrorList($lang); ?>